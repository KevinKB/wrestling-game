package game.moves;

import game.wrestlers.Wrestler;

public enum Move {
    PUNCH("Punch",5.0,1.0,5.0),
    KICK("Kick",7.0,1.5,7.0),
    SUPLEX("Suplex",12.0,2.0,15.0),
    POWERBOMB("PowerBomb",25.0,4.0,25.0)
;
    String Name;
    Double BaseDamage;
    Double BaseReversalChance;
    Double StaminaCost;
    private Move(String name, Double baseDamage, Double baseReversalChance,
        Double staminaCost) {
        Name = name;
        BaseDamage = baseDamage;
        BaseReversalChance = baseReversalChance;
        StaminaCost = staminaCost;
    }
    public String getName() {
        return Name;
    }
    public Double getBaseDamage() {
        return BaseDamage;
    }
    public Double getBaseReversalChance() {
        return BaseReversalChance;
    }
    public Double getStaminaCost() {
        return StaminaCost;
    }
    public Double CalculateMoveDamage(Wrestler w,Move m){
        Double damageCost;
        damageCost=(w.getPower()*m.getBaseDamage())/10.0;
        return damageCost;
        
    }
}
