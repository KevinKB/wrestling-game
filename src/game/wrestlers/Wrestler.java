package game.wrestlers;

public enum Wrestler {
    JOHNCENA("John Cena",8.5,8.5),
    BIGDAWG("Big Dawg",10.0,3.0),
    CRISPENWAH("Crispen Wah", 7.0,10.0)
    ;
String name;
Double power;
Double stamina;
Double reversals;
public String getName() {
    return name;
}
public Double getPower() {
    return power;
}
public Double getStamina() {
    return stamina;
}
public Double getReversals() {
    return reversals;
}
private Wrestler(String name, Double power, Double reversals) {
    this.name = name;
    this.power = power;
    this.reversals = reversals;
    this.stamina=100.0;
}
}
